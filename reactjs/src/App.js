import './App.css';
import AxiosGet from './components/AxiosGet';
// import AxiosPost from './components/AxiosPost';

function App() {
  return (
    <div className="App">
      {/* <AxiosPost /> */}
      <AxiosGet />
    </div>
  );
}

export default App;
