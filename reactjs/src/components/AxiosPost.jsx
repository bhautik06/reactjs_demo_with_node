import React, { Component } from "react";

class AxiosPost extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
    };
  }

  onChange = (e) => {
    this.setState({ username: e.target.value });
  };

  onSubmit = (e) => {
    e.preventDefault();
    console.log(`username: ${this.state.username}`);
  };

  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          Username:
          <input type="text" name="username" onChange={this.onChange} />
          <button type="submit">Submit</button>
        </form>
      </div>
    );
  }
}

export default AxiosPost;
