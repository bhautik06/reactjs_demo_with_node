import React, { Component } from "react";
import axios from "axios";

class AxiosGet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
    };
  }

  componentWillMount() {
    axios
      .get("http://localhost:4000")
      .then((res) => {
        // console.log(res.data)
        this.setState({
          users: res.data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  render() {
    return (
      <div>
        {this.state.users.map((data) => {
          return (
            <div key={data.id}>
              <h4>{data.username}</h4>
              <h4>{data.email}</h4>
              <h4>{data.password}</h4>
            </div>
          );
        })}
      </div>
    );
  }
}

export default AxiosGet;
