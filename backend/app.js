const express = require("express");
const route = express();
const cors = require("cors");
const port = 4000;

route.use(cors());
route.use(express.json());
route.use(express.urlencoded({ extended: false }));

const users = [];

route.get("/", (req, res) => {
  res.send(users);
});

route.post("/createUser", (req, res) => {
  const user = {
    id: users.length + 1,
    username: req.body.uname,
    email: req.body.email,
    password: req.body.pass,
  };
  users.push(user);
  res.send({ user: user, msg: "success" });
});

route.listen(port, () => console.log(`server running on port: ${port}`));
